#![warn(clippy::all, clippy::pedantic)]
#![allow(dead_code)]

struct Friend {
    id: u32,
    name: String,
    is_online: bool,
}

impl Friend {
    fn new(id: u32, name: String) -> Self {
        Self {
            id,
            name,
            is_online: false,
        }
    }
}

fn get_friends(friends: &mut Vec<Friend>) -> &Vec<Friend> {
    friends.push(Friend::new(1, "a".to_string()));

    friends
}

fn update_friend_name(friends: &mut Vec<Friend>, friend_id: u32, name: String) -> &Vec<Friend> {
    if let Some(x) = friends.iter_mut().find(|x| x.id == friend_id) {
        x.name = name;
    }

    friends
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn construct_new_friend() {
        let friend = Friend::new(1, "a".to_string());

        assert_eq!(friend.id, 1);
        assert_eq!(friend.name, "a");
        assert_eq!(friend.is_online, false);
    }

    #[test]
    fn get_friends_should_add_friends_to_mutable_vec() {
        let mut friends_vec: Vec<Friend> = Vec::new();

        let friends = get_friends(&mut friends_vec);

        assert_eq!(friends[0].name, "a");
    }

    #[test]
    fn update_friend_name_in_friends() {
        let mut friends_vec: Vec<Friend> = Vec::new();
        friends_vec.push(Friend::new(1, "a".to_string()));
        friends_vec.push(Friend::new(2, "b".to_string()));
        friends_vec.push(Friend::new(3, "c".to_string()));

        update_friend_name(&mut friends_vec, 3, "d".to_string());

        assert_eq!(friends_vec[2].name, "d");
    }
}
